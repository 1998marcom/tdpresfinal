# Presentazione finale di TD

[![build status](https://gitlab.com/1998marcom/tdpresfinal/badges/master/pipeline.svg)](https://gitlab.com/1998marcom/tdpresfinal/-/jobs/artifacts/master/raw/PresentazioneFinaleMalandrone.pdf?job=build)

Presentazione finale di TD. Ultimo pdf disponibile [qui](https://gitlab.com/1998marcom/tdpresfinal/-/jobs/artifacts/master/raw/PresentazioneFinaleMalandrone.pdf?job=build).

![CC-BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png "CC-BY-NC-SA")
(salvo ove espressamente diversamente indicato)